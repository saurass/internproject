<?php
/* Created By -- SAURASS
 * Date -- 29/12/2017
 * Place -- AKGEC SBH - 1
 * */
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome !</title>
    <script src="./AJAX/IndexAjax.js"></script>
    <script src="./JS/indexPage.js"></script>
    <script src="./JS/ValidateRegisterPage.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="./JS/bootstrap.min.js"></script>
    <script src="./JS/bootstrap.bundle.js"></script>
    <script src="./JS/bootstrap.bundle.min.js"></script>
    <script src="./JS/bootstrap.js"></script>

    <link rel="stylesheet" type="text/css" href="./CSS/index.css">
    <link rel="stylesheet" href="./CSS/bootstrap.min.css">
    <link rel="stylesheet" href="./CSS/bootstrap.css">
    <link rel="stylesheet" href="./CSS/bootstrap-grid.css">
    <link rel="stylesheet" href="./CSS/bootstrap-grid.min.css">
    <link rel="stylesheet" href="./CSS/bootstrap-reboot.css">
    <link rel="stylesheet" href="./CSS/bootstrap-reboot.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<div class="col-md-6 col-md offset-3" id="dd">
    <center>
        <div class="card-img-overlay" style="padding-top: 30%">
            <div class="text-center text-white text-capitalize text-justify"><h1>Welcome To WebNet</h1></div>
            <?php
                if(isset($_SESSION['username'])) {
                    ?>
                    <a href="./homePage.php"><button class="btn btn-outline-light">Home</button></a>
                    <?php
                }else {
                    ?>
                    <button class="btn btn-outline-light" onclick="onLogin()">Log In</button>
                    <button class="btn btn-outline-light" onclick="onReg()">Sign Up</button>
                    <?php
                }
            ?>
        </div>
    </center>
</div>
<?php
if (isset($_GET['error']) and $_GET['error'] == 'Success') {
    ?>
    <br>
    <div class="col-md-6 col-md offset-3">
        <div class="card">
            <div class="card-header">
                <div class="text-success"><h3>Successfully Registered !</h3></div>
            </div>
        </div>
    </div>
    <?php
}
?>
<?php
if (isset($_GET['error']) and $_GET['error'] == 'AlreadyRegistered') {
    ?>
    <br>
    <div class="col-md-6 col-md offset-3">
        <div class="card">
            <div class="card-header">
                <div class="text-danger"><h3>User Already Exists !</h3></div>
            </div>
        </div>
    </div>
    <?php
}
?>

<?php
if (isset($_GET['error']) and $_GET['error'] == 'InvalidCredintial') {
    ?>
    <br>
    <div class="col-md-6 col-md offset-3">
        <div class="card">
            <div class="card-header">
                <div class="text-danger"><h3>Invalid Login Credintials!</h3></div>
            </div>
        </div>
    </div>
    <?php
}
?>

<?php
if (isset($_GET['error']) and $_GET['error'] == 'InCompleteForm') {
    ?>
    <br>
    <div class="col-md-6 col-md offset-3">
        <div class="card">
            <div class="card-header">
                <div class="text-danger"><h3>Please Fill All Fields Properly !</h3></div>
            </div>
        </div>
    </div>

    <?php
}
?>

<?php
if (isset($_GET['error']) and $_GET['error'] == 'UserSmWr') {
    ?>
    <br>
    <div class="col-md-6 col-md offset-3">
        <div class="card">
            <div class="card-header">
                <div class="text-danger"><h3>Something Wrong With Your Account</h3></div>
            </div>
        </div>
    </div>
    <?php
}
?>

<?php
if (isset($_GET['error']) and $_GET['error'] == 'RegisterFirst') {
    ?>
    <br>
    <div class="col-md-6 col-md offset-3">
        <div class="card">
            <div class="card-header">
                <div class="text-info"><h3>Register First Please !</h3></div>
            </div>
        </div>
    </div>
    <?php
}
?>
<?php
if (isset($_GET['error']) and $_GET['error'] == 'smtwr') {
    ?>
    <br>
    <div class="col-md-6 col-md offset-3">
        <div class="card">
            <div class="card-header">
                <div class="text-danger"><h3>Oops! Somethng Went Wrong</h3></div>
            </div>
        </div>
    </div>
    <?php
}
?>
<?php
if (isset($_GET['error']) and $_GET['error'] == 'loginFirst') {
    ?>
    <br>
    <div class="col-md-6 col-md offset-3">
        <div class="card">
            <div class="card-header">
                <div class="text-info"><h3>Please Login First</h3></div>
            </div>
        </div>
    </div>
    <?php
}
?>
<?php
if (isset($_GET['error']) and $_GET['error'] == 'logoutSuccess') {
    ?>
    <br>
    <div class="col-md-6 col-md offset-3">
        <div class="card">
            <div class="card-header">
                <div class="text-success"><h3>Successfully Logged Out</h3></div>
            </div>
        </div>
    </div>
    <?php
}
?>
<div id="regOverlay">
    <div class="col-md-6 col-md offset-3">
        <div class="card">
            <div class="card-header">
                <div class="float-left"><h4>Register Here !!</h4></div>
                <div class="float-right"><a href="#" onclick="offReg()" class="text-dark"><i class="fa fa-times"
                                                                                             aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="card-body">
                <form action="./Backend/RegisterUser.php" method="post">
                    <label class="badge badge-primary">Name</label><br>
                    <input type="text" name="name" class="form-control" placeholder="Your Name Here Please"
                           required><br>

                    <label class="badge badge-primary">Username</label><br>
                    <input type="text" name="username" class="form-control" id="username"
                           placeholder="Please Create A unique Username"
                           onkeyup="checkUsername(this.value)" required>
                    <div class="text-info" id="check_user"></div>
                    <br>

                    <label class="badge badge-primary">Password</label><br>
                    <input type="password" name="password" class="form-control" id="pass"
                           placeholder="Please Create A Password" minlength="8"
                           required><br>
                    <label class="badge badge-primary">Comfirm Password</label><br>
                    <input type="password" name="cmfpassword" class="form-control" id="cmfpass"
                           placeholder="Please Renter The Password" onkeyup="checkPasswordMatch()" minlength="8"
                           required>
                    <div class="text-info" id="check_pass"></div>
                    <br>
                    <label class="badge badge-primary">Gender</label><br>
                    <select name="gender" class="form-control" required>
                        <option value="">SELECT</option>
                        <option value="M">Male</option>
                        <option value="F">Female</option>
                    </select><br>
                    <label class="badge badge-primary">Qualifications</label><br>
                    <div class="custom-checkbox"><input type="checkbox" name="HS">High School</div>
                    <div class="custom-checkbox"><input type="checkbox" name="SS">Senior Secondary</div>
                    <div class="custom-checkbox"><input type="checkbox" name="UG">Under Graduate</div>
                    <div class="custom-checkbox"><input type="checkbox" name="PG">Post Graduate<br></div>
                    <br>

                    <label class="badge badge-primary">Description</label><br>
                    <textarea name="description" class="form-control" placeholder="Please Tell Something About You"
                              required></textarea><br><br>
                    <input type="submit" class="btn btn-success form-control" onclick="return RegFormCheck()"
                           value="Register" name="register">
                </form>
            </div>
        </div>
    </div>
</div>

<div id="loginOverlay">
    <div class="col-md-6 col-md offset-3">
        <div class="card">
            <div class="card-header">
                <div class="float-left"><h4>Login Here !!</h4></div>
                <div class="float-right"><a href="#" onclick="offLogin()" class="text-dark"><i class="fa fa-times"
                                                                                               aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="card-body">
                <form action="./Backend/LoginUser.php" method="post">
                    <label class="badge badge-primary">Username</label><br>
                    <input type="text" name="username" class="form-control" placeholder="Enter Your Username"
                           required><br>

                    <label class="badge badge-primary">Password</label><br>
                    <input type="password" name="password" class="form-control" placeholder="Enter Your Password"
                           required><br>

                    <input type="submit" class="btn btn-primary form-control" value="Login" name="login">
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>