<?php
/**
 * Created by PhpStorm.
 * User: Saurass
 * Date: 29-12-2017
 */
session_start();
if (isset($_SESSION['username']))
{
    session_unset();
    header('Location:../index.php?error=logoutSuccess');
}
else{
    header('Location:../index.php?error=smtwr');
}