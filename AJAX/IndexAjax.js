/**
 * Created by Saurass on 29-12-2017.
 */

function checkUsername(user)
{
    if (user.length == 0)
    {
        document.getElementById("check_user").innerHTML = "";
        return;
    }
    else
    {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function()
        {
            if (this.readyState == 4 && this.status == 200)
            {
                document.getElementById("check_user").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET", "./AjaxPhp/checkUsername.php?user="+user, true);
        xmlhttp.send();
    }
}
