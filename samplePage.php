<?php
/**
 * Created by PhpStorm.
 * User: Saurass
 * Date: 29-12-2017
 */
?>
<html>
<head lang="en">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sample Design Page</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="./JS/bootstrap.min.js"></script>
    <script src="./JS/bootstrap.bundle.js"></script>
    <script src="./JS/bootstrap.bundle.min.js"></script>
    <script src="./JS/bootstrap.js"></script>
    <script src="./JS/samplePage.js"></script>

    <link rel="stylesheet" href="./CSS/bootstrap.min.css">
    <link rel="stylesheet" href="./CSS/bootstrap.css">
    <link rel="stylesheet" href="./CSS/bootstrap-grid.css">
    <link rel="stylesheet" href="./CSS/bootstrap-grid.min.css">
    <link rel="stylesheet" href="./CSS/bootstrap-reboot.css">
    <link rel="stylesheet" href="./CSS/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="./CSS/samplePage.css">
</head>

<body style="overflow: hidden">
<nav class="navbar navbar-expand-md navbar-dark" style="background-color: #868e96">
    <a class="navbar-brand" href="./index.php"><h1>Exam Preparation Online</h1></a>
</nav>

<div style="overflow: scroll;height: 90%">
    <div class="col-md-4 float-left"
         style="padding-top: 2%;">

        <div id="checkLine" class="border-left-0 border-top-0 border-bottom-0"
             style="margin-top: 1%;border-style: solid;border-width: 1px;height: 100%">
            <div class="card card-header" style="background-color: #e0dada;margin-right: 5%">
                <div><label class="float-right small"><a href="#">Apply Now</a></label></div>
                <div><label>Organisation:</label></div>
                <div>
                    <select class="form-control" required>
                        <option value="">SELECT</option>
                        <option value="">AWS</option>
                        <option value="">BMW</option>
                        <option value="">AUDI</option>
                    </select>
                </div>
                <br>

                <div><label>Engineering Stream:</label></div>
                <div>
                    <select class="form-control" required>
                        <option value="">SELECT</option>
                        <option value="">CSE</option>
                        <option value="">ECE</option>
                        <option value="">IT</option>
                        <option value="">ME</option>
                    </select>
                </div>
                <br>

                <div><label>Application Mode:</label></div>
                <div>
                    <select class="form-control" required>
                        <option value="">SELECT</option>
                        <option value="">Online</option>
                        <option value="">Offline</option>
                        <option value="">Mail</option>
                        <option value="">Post</option>
                    </select>
                </div>
                <br>
            </div>
            <br>

            <div class="card card-header" style="background-color: #e0dada;margin-right: 5%">
                <div><label class="float-right small"><a onclick="clearAll()" href="#">Clear All</a> / <a onclick="selectAll()" href="#">Select All</a></label>
                </div>
                <div><input type="checkbox" name="nm1" id="nm1" class="custom-checkbox">&nbsp;<label>Selection Procedure</label></div>
                <div><input type="checkbox" name="nm2" id="nm2" class="custom-checkbox">&nbsp;<label>Technical Interview</label></div>
                <div><input type="checkbox" name="nm3" id="nm3" class="custom-checkbox">&nbsp;<label>Analytical Questions</label></div>
                <div><input type="checkbox" name="nm4" id="nm4" class="custom-checkbox">&nbsp;<label>HR Questions</label></div>
                <div><input type="checkbox" name="nm5" id="nm5" class="custom-checkbox">&nbsp;<label>Suggestions</label></div>

            </div>
        </div>

    </div>

    <div class="float-left col-md-8" style="margin-top: 4%;margin-bottom: 4%"><h1>INTERVIEW EXPERIENCES :: ARW</h1></div>

    <div class="float-left col-md-7" style="border-style: solid;border-width: 1px;margin-left: 1.5%">
        <div style="margin-left: 5%;margin-right: 5%;margin-top: 5%;margin-bottom: 5%">
            Perceived end knowledge certainly day sweetness why cordially. Ask quick six seven offer see among. Handsome met debating sir dwelling age material. As style lived he worse dried. Offered related so visitor we private removed. Moderate do subjects to distance.

            Manor we shall merit by chief wound no or would. Oh towards between subject passage sending mention or it. Sight happy do burst fruit to woody begin at. Assurance perpetual he in oh determine as. The year paid met him does eyes same. Own marianne improved sociable not out. Thing do sight blush mr an. Celebrated am announcing delightful remarkably we in literature it solicitude. Design use say piqued any gay supply. Front sex match vexed her those great.

            Neat own nor she said see walk. And charm add green you these. Sang busy in this drew ye fine. At greater prepare musical so attacks as on distant. Improving age our her cordially intention. His devonshire sufficient precaution say preference middletons insipidity. Since might water hence the her worse. Concluded it offending dejection do earnestly as me direction. Nature played thirty all him.
        </div>
    </div>

    <div class="float-left col-md-7" style="border-style: solid;border-width: 1px;margin-left: 1.5%;margin-top: 2%;margin-bottom: 2%">
        <div style="margin-left: 5%;margin-right: 5%;margin-top: 5%;margin-bottom: 5%">
            Perceived end knowledge certainly day sweetness why cordially. Ask quick six seven offer see among. Handsome met debating sir dwelling age material. As style lived he worse dried. Offered related so visitor we private removed. Moderate do subjects to distance.

            Manor we shall merit by chief wound no or would. Oh towards between subject passage sending mention or it. Sight happy do burst fruit to woody begin at. Assurance perpetual he in oh determine as. The year paid met him does eyes same. Own marianne improved sociable not out. Thing do sight blush mr an. Celebrated am announcing delightful remarkably we in literature it solicitude. Design use say piqued any gay supply. Front sex match vexed her those great.

            Neat own nor she said see walk. And charm add green you these. Sang busy in this drew ye fine. At greater prepare musical so attacks as on distant. Improving age our her cordially intention. His devonshire sufficient precaution say preference middletons insipidity. Since might water hence the her worse. Concluded it offending dejection do earnestly as me direction. Nature played thirty all him.
        </div>
    </div>

</div>

</body>
</html>
