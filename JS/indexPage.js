/**
 * Created by Saurass on 29-12-2017.
 */
function checkPasswordMatch()
{
    var pass=document.getElementById('pass').value;
    var cmfpass=document.getElementById('cmfpass').value;
    if (pass==cmfpass)
    {
        document.getElementById('check_pass').innerHTML="Password Match";
    }
    else
    {
        document.getElementById('check_pass').innerHTML="Password Not Match";
    }
}

function onLogin() {
    document.getElementById('loginOverlay').style.display = "block";
    return false;
}

function onReg() {
    document.getElementById('regOverlay').style.display = "block";
    return false;
}

function offLogin() {
    document.getElementById('loginOverlay').style.display = "none";
    return false;
}

function offReg() {
    document.getElementById('regOverlay').style.display = "none";
    return false;
}
