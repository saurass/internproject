/**
 * Created by Saurass on 29-12-2017.
 */

function RegFormCheck()
{
    var username_status=document.getElementById('check_user').innerText;
    var pass_status=document.getElementById('check_pass').innerText;

    var username=document.getElementById('username').value;
    var password=document.getElementById('pass').value;

    if(username_status=='Good To Go !!' && pass_status=='Password Match')
    {
        if(username.length<6)
        {
            document.getElementById('check_user').innerText='Minimum 6 Character Username Required';
            return false;
        }
        if(password.length<8)
        {
            document.getElementById('check_pass').innerText='Minimum 8 Character Password Required';
            return false;
        }
        return true;
    }
    else
        return false;
}
